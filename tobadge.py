import serial
import argparse
import os

parser = argparse.ArgumentParser(description='copy some bytes.')
parser.add_argument('--inputegg', help='egg-folder to copy from (src)')
parser.add_argument('--input', help='file to copy from (src)')
parser.add_argument('--output', help='file to copy from (optional, default: x2.elf)')


args = parser.parse_args()

out_file=b'x2.elf' # put in apps (e.g. apps/t.py) to see in menu 
in_file = '../target/thumbv7em-none-eabi/release/l0dable-example'

egg = False
if args.input is not None:
    in_file = args.input
if args.output is not None :
    out_file = args.output.encode()
if args.inputegg is not None :
    inputegg = args.inputegg
    egg = True

def cp_file(in_file, out_file):
    with open(in_file,'rb') as f:
     
        inp = f.read()
        
        with serial.Serial('/dev/ttyACM0',9600) as s:
            s.write(b"import os\r\n") 

            # remove old out_file
            s.write(b"os.rename('" + out_file + b"','rm-'  + str(int(os.urandom(1)[0]))  + str(int(os.urandom(1)[0]))  + str(int(os.urandom(1)[0]))  + str(int(os.urandom(1)[0])) + str(int(os.urandom(1)[0]))  )\r\n") 
            s.write(b"of = open('"+ out_file + b"','w')\r\n")
            
            bsize = 8
            for si in range(1 + (len(inp)//bsize)):
                i = si * bsize
                cmd = b'c=[' 
                for j in range(bsize):
                    if i+j < len(inp):
                        cmd += str(int(inp[i+j])).encode() + b","
                cmd += b']\r\n'
                s.write(cmd)
                s.flush()
                s.write(b'of.write(bytes(c))\r\n')
                s.flush() 
                print(i+j,len(inp))
            
            s.write(b'of.close()\r\n')
            s.flush()
            s.write(b'import os\r\n')
            s.flush()
            #s.write(b'os.exec("'+out_file+ b'")\r\n')
            #s.flush()


def run(out_file):
    with serial.Serial('/dev/ttyACM0',9600) as s:
        s.write(b'os.exec("'+out_file+ b'")\r\n')
        s.flush()

def setup_egg(inputegg):
    egg_name = os.path.basename(inputegg)

    with serial.Serial('/dev/ttyACM0',9600) as s:
        s.write(b"import os\r\n")
        s.write(b"os.mkdir('apps/" + egg_name.encode() +   b"') \r\n")
    return egg_name

if egg:
    egg_name = setup_egg(inputegg)
    for f in os.listdir(inputegg):
        print('copy: ', f)
        cp_file(os.path.join(inputegg, f) ,b'apps/'+egg_name.encode() +b'/'+ f.encode())

    
else: 
    cp_file(in_file, out_file)
    run(out_file)

